import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    Calculator calculator = new Calculator();
    @Test
    void addTest() {
        assertEquals(12, calculator.add(7, 5));
    }
    @Test
    void subtractTest() {
        assertEquals(-4, calculator.subtract(6, 10));
    }
    @Test
    void multiplyTest() {
        assertEquals(56, calculator.multiply(7, 8));
    }
    @Test
    void divideTest() {
        assertEquals(2.5, calculator.divide(5,2));
    }
    @Test
    void divideByZeroTest() throws ArithmeticException{
        calculator.divide(4, 0);
    }

}