import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Main {
    public static void main(String[] args) {
        WebDriver driver = new FirefoxDriver();

        driver.get("https://ts1.v-sources.eu");

        driver.findElement(By.id("flight_form_firstName")).sendKeys("Anna");

        Select selectDestination = new Select(driver.findElement(By.id("flight_form_destination")));
        selectDestination.selectByVisibleText("Paris");

        driver.findElement(By.cssSelector("body > div > div > form > div > button")).click();
    }
}
