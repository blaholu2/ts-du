# TS DU



## Obsah repositáře

Tento repositář obsahuje mé domácí úkoly a práci ze cvičení. Tyto soubory jsou rozděléné do dvou stejnojmenných složek.
Složky obsahující domácí úkoly jsou buď očíslovány podle pořadí v Moodle, nebo jsou pojmenovány podle instrukcí z Moodle.
Složky s prácí ze cvičení jsou očíslovány podle cvičení, na kterém byla práce zadána.

